/**
 * Head de vue-meta
 * @type {Object}
 * @url https://github.com/declandewet/vue-meta
 */
const title = 'SummerHills'
const description = 'SummerHills'
const url = 'http://cursodeverano.summerhills.com.mx/'

const icons = require('./icons')({ title })
const og = require('./og-data')({ title, url, description })
const schema = require('./schema')({ title, url, description })

/**
 * Propiedades meta de html
 * @type {Array} meta
 */
const meta = [
  { charset: 'utf-8' },
  // eslint-disable-next-line
  { name: 'keywords', content: 'SummerHills, Escuela, colonia condesa, educación básica, guardería, montesori' },
  { name: 'viewport', content: 'width=device-width, initial-scale=1, minimum-scale=1' },
  { hid: 'description', name: 'description', content: description },
  { name: 'google-site-verification', content: '' },
]

const link = [
  {
    href: 'https://fonts.googleapis.com/css?family=Quicksand|Rubik:400,700',
    rel: 'stylesheet',
  },
]


meta.splice(2, 0, ...og)
meta.splice(2, 0, ...icons.meta)
link.splice(2, 0, ...icons.link)

const head = {
  // If undefined or blank then we don't need the hyphen
  titleTemplate: titleChunk => (titleChunk ? `${titleChunk} - SummerHills` : 'SummerHills'),
  link,
  meta,
  script: [
    { innerHTML: JSON.stringify(schema), type: 'application/ld+json' },
  ],
  noscript: [
    { innerHTML: 'Para tener una experiencia óptima, habilita JavaScript en tu navegador.' },
  ],
  __dangerouslyDisableSanitizers: ['script'],
}

module.exports = head
