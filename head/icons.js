// Obtenidos de https://realfavicongenerator.net
module.exports = function icons(newOptions) {
  const options = Object.assign({
    title: 'SummerHills',
    color: '#211f35',
  }, newOptions)

  return {
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'apple-touch-icon', sizes: '180x180', href: '/apple-touch-icon.png' },
      {
        rel: 'icon', type: 'image/png', sizes: '32x32', href: '/favicon-32x32.png',
      },
      {
        rel: 'icon', type: 'image/png', sizes: '16x16', href: '/favicon-16x16.png',
      },
      { rel: 'manifest', href: '/manifest.json' },
      { rel: 'mask-icon', href: '/safari-pinned-tab.svg', color: '#f83f28' },
      { rel: 'shortcut icon', href: '/favicon.ico' },
    ],
    meta: [
      { name: 'apple-mobile-web-app-title', content: options.title },
      { name: 'application-name', content: options.title },
      { name: 'msapplication-TileColor', content: options.color },
      { name: 'msapplication-TileImage', content: '/mstile-144x144.png' },
      { name: 'theme-color', content: options.color },
    ],
  }
}
