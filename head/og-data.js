module.exports = function metadata(newOptions) {
  const options = Object.assign({
    title: 'SummerHills',
    url: 'http://cursodeverano.summerhills.com.mx/',
    description: 'SummerHills',
  }, newOptions)
  return [
    {
      hid: 'fbapp_id',
      property: 'fb:app_id',
      content: '2004847559786991',
    },
    {
      hid: 'fb:app_id',
      property: 'fb:pages',
      content: '187029654704725',
    },
    {
      hid: 'og:title',
      property: 'og:title',
      content: options.title,
    },
    {
      hid: 'og:type',
      property: 'og:type',
      content: 'website',
    },
    {
      hid: 'og:url',
      property: 'og:url',
      content: options.url,
    },
    {
      property: 'og:locale',
      cotent: 'es_MX',
    },
    {
      property: 'og:site_name',
      cotent: options.title,
    },
    {
      hid: 'og:description',
      property: 'og:description',
      content: options.description,
    },
    {
      property: 'og:image',
      content: 'http://cursodeverano.summerhills.com.mx/images/social.png',
    },
    {
      property: 'og:image:secure_url',
      content: 'http://cursodeverano.summerhills.com.mx/images/social.png',
    },
    {
      property: 'og:image:type',
      content: 'image/jpeg',
    },
    {
      property: 'og:image:width',
      content: '1192',
    },
    {
      property: 'og:image:height',
      content: '715',
    },
    {
      property: 'og:image:alt',
      cotent: 'Somos una agencia que se preocupa por tu imagen y tus clientes',
    },
  ]
}
