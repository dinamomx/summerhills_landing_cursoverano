/**
 * Schema.org Structured Data
 * @type {Object}
 * @link https://jsonld.com/
 * @link https://searchengineland.com/schema-markup-structured-data-seo-opportunities-site-type-231077
 * @link https://docs.google.com/spreadsheets/d/1Ed6RmI01rx4UdW40ciWgz2oS_Kx37_-sPi7sba_jC3w/edit#gid=0
 */

const address = {
  '@type': 'PostalAddress',
  addressLocality: 'Condesa',
  addressRegion: 'CDMX',
  postalCode: '04450',
  streetAddress: 'Yautepec Núm. 72',
}
const sameAs = [
  'https://www.facebook.com/MitsubishiSamurai/',
  'https://www.youtube.com/results?search_query=mitsubishi+samurai+universidad',
]

module.exports = function schema(newOptions) {
  const options = Object.assign({
    sameAs,
    address,
    name: 'SummerHills',
    title: 'SummerHilss, Más que una escuela una forma de vida',
    url: 'http://dinamo.mx',
    description: 'Más que una escuela, una forma de vivir',
  }, newOptions)
  return {
    '@context': 'http://schema.org',
    '@graph': [
      // LocalBusiness
      {
        '@type': 'LocalBusiness',
        address,
        name: options.name,
        description: options.description,
        openingHours: [
          'Mo-Fr 9:00-19:00',
        ],
        telephone: '55 4140 8714',
        url: options.url,
        logo: {
          '@context': 'http://schema.org',
          '@type': 'ImageObject',
          author: 'Dinamo',
          contentLocation: 'Ciudad de Mexico, Mexico',
          contentUrl: 'https://dinamo.mx/logo-dinamo.jpg',
          description: options.description,
          name: options.name,
        },
        image: 'https://dinamo.mx/logo-dinamo.jpg',
        geo: {
          '@type': 'GeoCoordinates',
          latitude: '19.345492',
          longitude: '-99.2165958',
        },
        hasMap: 'https://www.google.com.mx/maps/place/Dinamo+agencia+de+comunicaci%C3%B3n/@19.345492,-99.2165958,12.21z/data=!4m8!1m2!2m1!1sdinamo!3m4!1s0x85ce01d6e0f8a7ad:0xbdd6e1bf9dbd8b53!8m2!3d19.334624!4d-99.137503',
        sameAs,
      },
      // Organization
      {
        '@type': 'Organization',
        name: options.name,
        legalName: options.name,
        url: options.url,
        logo: 'https://dinamo.mx/logo-dinamo.jpg',
        foundingDate: '2009',
        founders: [
          {
            '@type': 'Person',
            name: 'Ramses Reyes',
          },
          {
            '@type': 'Person',
            name: 'Seth Gonzalez',
          }],
        address,
        contactPoint: {
          '@type': 'ContactPoint',
          contactType: 'Sales',
          telephone: '+52 55 4140 8714',
          email: 'tanya@wdinamo.com',
        },
        sameAs,
      },
      {
        '@type': 'WebSite',
        url: options.url,
        name: options.title,
        author: {
          '@type': 'Organization',
          name: options.name,
        },
        description: options.description,
        publisher: options.name,
        // potentialAction: {
        //   '@type': 'SearchAction',
        //   target: 'http://www.example.com/?s={search_term}',
        //   'query-input': 'required name=search_term',
        // },
      },
    ],
  }
}
