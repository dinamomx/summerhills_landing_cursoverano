module.exports = {
  root: true,
  env: {
    browser: true,
    node: true,
    es6: true
  },
  parserOptions: {
    parser: 'babel-eslint',
    ecmaVersion: 6,
    sourceType: 'module'
  },
  extends: [
    'airbnb-base',
    'plugin:vue/recommended'
  ],
  // required to lint *.vue files
  plugins: [
    // 'html'
  ],
  settings: {
    'import/resolver': {
      webpack: 'webpack.config.js',
    }
  },
  // add your custom rules here
  rules: {
    semi: ["error", "never"],
    'no-console': 1,
    'no-debugger': 1,
    'import/extensions': ['error', 'always', {
      js: 'never',
      vue: 'never'
    }],
    'max-len': ['error', 969],
    'no-plusplus': ["error", { "allowForLoopAfterthoughts": true }],
    'no-param-reassign': ["error", { "props": false }],
    // 'no-use-before-define': 0,
  }
}
