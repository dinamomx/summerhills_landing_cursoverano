// Esta configuración está como placeholder en lo que se actualiza el plugin nuxtent
/* eslint-disable import/no-unresolved */
/* eslint-disable import/no-extraneous-dependencies */

const markdownContainer = require('markdown-it-container')

module.exports = {
  content: [
    ['projects',
      {
        page: '/projects/_slug',
        permalink: '/projects/:slug',
        // path: '/projects/:slug',
        isPost: false,
        generate: ['get', 'getAll'],
      }],
  ],
  parsers: {
    md: {
      use: [
        [markdownContainer, 'section', {
          validate(params) {
            // console.log('Val', params, params.trim().match(/^section\s+(.*)$/))
            return params.trim().match(/^section\s+(is-text|is-image|is-gallery|is-large|is-medium)(.*)$/)
          },
          render(tokens, idx) {
            const m = tokens[idx].info.trim().match(/^section\s+(is-text|is-image|is-gallery|is-large|is-medium)(.*)$/)
            // console.log('Section', m, tokens[idx])
            if (tokens[idx].nesting === 1) {
              // opening tag
              return `<section class="section ${m[1]}">
              <div class="container">
              ${m[2]}`
            }
            // closing tag
            return '</div></section>'
          },
        }],
      ],
    },
  },
}
